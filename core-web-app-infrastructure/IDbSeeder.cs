﻿using System;
using System.Threading.Tasks;

namespace core_web_app_infrastructure
{
    public interface IDbSeeder
    {
        Task Seed();
    }
}
