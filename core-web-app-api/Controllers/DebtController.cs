﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using core_web_app_bl;
using core_web_app_viewmodel;

namespace core_web_app_api.Controllers
{
    [Produces("application/json")]
    [Route("api/Debt")]
    public class DebtController : BaseController
    {
        private readonly DebtDomainModel _domainModel;

        public DebtController(DebtDomainModel domainModel) : base()
        {
            _domainModel = domainModel;
        }

        [HttpGet]
        public IEnumerable<DebtViewModel> Get()
        {
            return _domainModel.GetDebts();
        }

        [HttpPost]
        public DebtViewModel Post(DebtViewModel model)
        {
            var id = _domainModel.SaveDebt(model);
            return _domainModel.GetDebt(id);
        }
        [HttpPut]
        public DebtViewModel Put(DebtViewModel model)
        {
            var id = _domainModel.SaveDebt(model);
            return _domainModel.GetDebt(id);
        }
    }
}