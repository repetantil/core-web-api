﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using core_web_app_bl;
using core_web_app_viewmodel;

namespace core_web_app_api.Controllers
{
    [Produces("application/json")]
    [Route("api/Currency")]
    public class CurrencyController : BaseController
    {
        private readonly DebtDomainModel _domainModel;
        public CurrencyController(DebtDomainModel domainModel)
        {
            _domainModel = domainModel;
        }

        [HttpGet]
        public IEnumerable<CurrencyViewModel> Get()
        {
            return _domainModel.GetCurrencies();
        }
    }
}