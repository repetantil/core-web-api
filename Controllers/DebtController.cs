using System.Collections.Generic;
using debt_core_app.BL;
using debt_core_app.DAL;
using debt_core_app.Model;
using Microsoft.AspNetCore.Mvc;

namespace debt_core_app.Controllers
{
    public class DebtController:BaseController
    {

        public DebtController(DebtContext context) : base(context)
        {
        }

        [HttpGet]
        public IEnumerable<DebtViewModel> Get()
        {
            return new DebtDomainModel(_context).GetDebts();
        }
        
        [HttpPost]
        public DebtViewModel Post(DebtViewModel model)
        {
            var dm = new DebtDomainModel(_context);
            var id = dm.SaveDebt(model);
            return dm.GetDebt(id);
        }
        [HttpPut]
        public DebtViewModel Put(DebtViewModel model)
        {
            var dm = new DebtDomainModel(_context);
            var id = dm.SaveDebt(model);
            return dm.GetDebt(id);
        }
    }
}