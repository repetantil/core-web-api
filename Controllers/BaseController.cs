using debt_core_app.DAL;
using Microsoft.AspNetCore.Mvc;

namespace debt_core_app.Controllers
{
    public class BaseController:Controller
    {
        protected readonly DebtContext _context;
        public BaseController(DebtContext context)
        {
            _context = context;
        }
    }
}