using System.Collections.Generic;
using debt_core_app.BL;
using debt_core_app.DAL;
using debt_core_app.Model;
using Microsoft.AspNetCore.Mvc;

namespace debt_core_app.Controllers
{
    [Route("api/[controller]")]
    public class CurrencyController:BaseController
    {
        public CurrencyController(DebtContext context) : base(context)
        {
        }

        [HttpGet]
        public IEnumerable<CurrencyViewModel> Get()
        {
            return new DebtDomainModel(_context).GetCurrencies();
        }
    }
}