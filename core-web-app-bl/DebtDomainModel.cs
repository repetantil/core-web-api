using core_web_app_dal;
using core_web_app_viewmodel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace core_web_app_bl
{
    public class DebtDomainModel
    {
        private readonly DebtContext _context;
        public DebtDomainModel(DebtContext context)
        {
            _context = context;
        }
        public IEnumerable<CurrencyViewModel> GetCurrencies()
        {
            return GetCurrecy().ToList();
        }

        public void SaveCurrecy(CurrencyViewModel currecy)
        {
            var exists = _context.Currencies.Any(x=>x.Name == currecy.Name);
            Currency entity = null;
            if(exists){
                entity = _context.Currencies.First(x=>x.Name == currecy.Name);
            }
            else{
                entity = new Currency();
            }
            entity.Name = currecy.Name;
            entity.Rate = currecy.Rate;
            entity.RateCompareTo = _context.Currencies.Where(x=>x.Name == currecy.RateCompareTo).FirstOrDefault();

            if(exists){
                _context.Update(entity);
            }
            else{
                _context.Add(entity);
            }
            _context.SaveChanges();
        }

        public IEnumerable<DebtViewModel> GetDebts(bool? isActive = true){
            return GetDebts().Where(x=>isActive.HasValue? x.IsActive == isActive.Value:true).ToList();
        }
        public DebtViewModel GetDebt(int id){
            return GetDebts().Where(x=>x.Id == id).FirstOrDefault();
        }

        public int SaveDebt(DebtViewModel debt){
            var isNew = false;
            var entity = _context.Debts.FirstOrDefault(x=>x.Id == debt.Id);
            if(entity == null){
                entity = new Debt();
                isNew = true;
            }
            entity.Amount = debt.Amount;
            entity.Currency = _context.Currencies.FirstOrDefault(x=>x.Id == debt.Currency.Id);
            entity.DateAdded = debt.DateAdded;
            entity.DateDeactivated = debt.DateDeactivated;
            entity.IsActive = debt.IsActive;
            entity.Note = debt.Note;
            entity.To = debt.To;
            if(isNew){
                _context.Add(entity);
            }
            else{
                _context.Update(entity);
            }
            _context.SaveChanges();
            return entity.Id;
        }

        public IQueryable<DebtViewModel> GetDebts(){
            return _context.Debts.Select(x=>new DebtViewModel(){
                Amount = x.Amount,
                Currency = GetCurrecy().FirstOrDefault(y=>y.Id == x.CurrencyId),
                DateAdded = x.DateAdded,
                DateDeactivated = x.DateDeactivated,
                Id = x.Id,
                IsActive = x.IsActive,
                Note = x.Note,
                To = x.To
            });
        }

        protected IQueryable<CurrencyViewModel> GetCurrecy(){
            return _context.Currencies.Select(x=> new CurrencyViewModel(){
                Id = x.Id,
                Name = x.Name,
                Rate = x.Rate,
                RateCompareTo = x.RateCompareTo ==null? string.Empty: x.RateCompareTo.Name
            });
        }
    }
}