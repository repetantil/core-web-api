﻿using core_web_app_dal;
using core_web_app_infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace core_web_app_bl
{
    public class ApplicationBootstrap
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DebtContext>(opt => opt.UseInMemoryDatabase("Debts"));
            services.AddTransient<IDbSeeder, DebtContextInitializer>();
            services.AddTransient<DebtDomainModel>();
        }

        public static void Configure(IDbSeeder seeder)
        {
            seeder.Seed().Wait();
        }
    }
}
