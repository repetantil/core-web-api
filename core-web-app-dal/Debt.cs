using System;

namespace core_web_app_dal
{
    public class Debt
    {
        public int Id { get; set; }
        public string To { get; set; }
        public int CurrencyId { get; set; }
        public decimal Amount { get; set; }
        public DateTime DateAdded { get; set; }
        public string Note { get; set; }
        public Currency Currency { get; set; }
        public bool IsActive { get; set; }
        public DateTime? DateDeactivated { get; set; }
    }
}