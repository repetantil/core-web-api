using System.Threading.Tasks;
using System.Linq;
using System;
using core_web_app_infrastructure;

namespace core_web_app_dal
{
    public class DebtContextInitializer: IDbSeeder
    {
        private readonly DebtContext _context;
        public DebtContextInitializer(DebtContext context)
        {
            _context = context;
        }

        public async Task Seed()
        {
            if(!_context.Currencies.Any())
            {
                var cUsd = new Currency(){
                    Name="USD",
                    Rate = 1
                };
                var cEu = new Currency(){
                    Name="EU",
                    Rate = 1.2M,
                    RateCompareTo = cUsd
                };
                var cGrn = new Currency(){
                    Name="GRN",
                    Rate = 32,
                    RateCompareTo = cEu
                };

                _context.Add(cUsd);
                _context.Add(cEu);
                _context.Add(cGrn);

                await _context.SaveChangesAsync();
            }

            if(!_context.Debts.Any())
            {
                var cUsd = new Debt(){
                    Amount = 1,
                    Currency = _context.Currencies.Where(x=>x.Name=="USD").FirstOrDefault(),
                    To="me usd",
                    DateAdded = DateTime.UtcNow,
                    Note="test1"
                };
                var cEu = new Debt(){
                    Amount = 1,
                    Currency = _context.Currencies.Where(x=>x.Name=="EU").FirstOrDefault(),
                    To="me euro",
                    DateAdded = DateTime.UtcNow,
                    Note="test1"
                };
                var cGrn = new Debt(){
                    Amount = 1,
                    Currency = _context.Currencies.Where(x=>x.Name=="GRN").FirstOrDefault(),
                    To="me grn",
                    DateAdded = DateTime.UtcNow,
                    Note="test1"
                };
                _context.Add(cUsd);
                _context.Add(cEu);
                _context.Add(cGrn);

                await _context.SaveChangesAsync();
            }
        }

    }
}