using System.Collections.Generic;

namespace core_web_app_dal
{
    public class Currency
    {
        public int Id { get; set; }
        public decimal Rate { get; set; }
        public string Name { get; set; }
        public int RateCompareToId { get; set; }
        public Currency RateCompareTo { get; set; }
        public List<Debt> Debts { get; set; }
    }
}