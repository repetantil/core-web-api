using Microsoft.EntityFrameworkCore;

namespace core_web_app_dal
{
    public class DebtContext: DbContext
    {
        public DebtContext(DbContextOptions<DebtContext> options):base(options)
        {
            
        }

        public DbSet<Currency> Currencies { get; set; } 
        public DbSet<Debt> Debts { get; set; } 


    }
}