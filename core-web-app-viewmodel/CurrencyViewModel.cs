namespace core_web_app_viewmodel
{
    public class CurrencyViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public string RateCompareTo { get; set; }
    }
}