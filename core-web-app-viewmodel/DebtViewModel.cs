using System;

namespace core_web_app_viewmodel
{
    public class DebtViewModel
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public string To { get; set; }
        public CurrencyViewModel Currency { get; set; }
        public DateTime DateAdded { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
        public DateTime? DateDeactivated { get; set; }
    }
}